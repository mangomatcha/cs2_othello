#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    s = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	b.~Board();
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
 Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    //identify opponent's color
    Side o;
    if (s == WHITE){ o = BLACK;}
	else if (s == BLACK){ o = WHITE;}
	
	//implement opponent's move (if opponent did not pass)
    if (opponentsMove == NULL) {}
    else{
		b.doMove(opponentsMove, o);
	}
	
	//deconstruct Player if game is over
	if(b.isDone()) 
	{ 
		b.~Board();
		return NULL;
	}
	
	//if there are no legal moves, return NULL
	if (!b.hasMoves(s)) { return NULL;}
	//if we are out of time, return NULL
	//else if (msLeft == 0) { return NULL;}
	else{
		//create a list of moves to store legal moves of player
		std::list<Move> legalMovesP;
		//iterate through all possible moves, adding them to legalMoves
		//if the move is legal
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				Move move(i, j);
				if (b.checkMove(&move, s))
				{
					legalMovesP.push_back(move);
				}
			}
		}
		//set up variables to use when comparing minimum score of each branch
		Board * b1, * b2;
		Move * max = &(*(legalMovesP.begin()));
		max->score = -1000000;
		//iterate through legal moves to find max
		for (std::list<Move>::iterator iter = legalMovesP.begin(); iter != legalMovesP.end(); iter++)
		{
			//copy boards into new variables and implement moves to compare
			b1 = b.copy();
			b2 = b.copy();
			b1->doMove(&(*iter), s);
			b2->doMove(max, s);
			
			//create list of legal moves for new board
			std::list<Move> legalMovesO;
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					Move move(i, j);
					if (b1->checkMove(&move, s))
					{
						legalMovesO.push_back(move);
					}
				}
			}
			
			//set up variables to use to compare scores
			Board * b3, * b4;
			Move * min = &(*(legalMovesO.begin()));
			min->score = 1000000;
			//iterate through legal moves of opponent to find min
			for (std::list<Move>::iterator it = legalMovesO.begin(); it != legalMovesO.end(); it++)
			{
				b3 = b1->copy();
				b4 = b1->copy();
				b3->doMove(&(*it), s);
				b4->doMove(min, s);
				
				//assign weights to scores based on board position
				if((it->x == 0 && it->y == 0) || (it->x == 0 && it->y == 7) || (it->x == 7 && it->y == 0) || (it->x == 7 && it->y == 7))
				{
					it->score = 3 * b3->count(s);
				}
				else if((it->x == 1 && it->y == 1) || (it->x == 1 && it->y == 6) || (it->x == 6 && it->y == 1) || (it->x == 6 && it->y == 6))
				{
					it->score = -3 * b3->count(s);
				}
				else if((it->x == 0 && (it->y == 1 || it->y == 6)) || (it->x == 7 && (it->y == 1 || it->y == 6)) || (it->y == 0 && (it->x == 1 || it->x == 6)) || (it->y == 7 && (it->x == 1 || it->x == 6)))
				{
					it->score = -2 * b3->count(s);
				}
				else if(it->x == 0 || it->y == 0 || it->x == 7 || it->y == 7)
				{
					it->score = 2 * b3->count(s);
				}
				else if(it->x == 1 || it->y == 1 || it->x == 6 || it->y == 6)
				{
					it->score = -2 * b3->count(s);
				}
				else
				{
					it->score = b3->count(s);
				}
				
				//compare scores of the opponent's moves and set the
				//pointer to the move with the lowest score equal to min
				if(it->score <= min->score) { min = &(*it);}	
			}
			//compare scores of the player's moves and set the pointer
			//to the move with the highest minimum score equal to max
			iter->score = min->score;
			if (iter->score >= max->score) { max = &(*iter);}		
		}
		//return the player move in LegalMovesP with the best minimax score
		return max;
	}	
}
 
 
 
 
 //older code without minimax algorithm
 

 /*Move *Player::doMove(Move *opponentsMove, int msLeft) {
 
    //identify opponent's color
    Side o;
    if (s == WHITE){ o = BLACK;}
	else if (s == BLACK){ o = WHITE;}
	
	//implement opponent's move (if opponent did not pass)
    if (opponentsMove == NULL) {}
    else{
		b.doMove(opponentsMove, o);
	}
	
	//deconstruct Player if game is over
	if(b.isDone()) 
	{ 
		b.~Board();
		return NULL;
	}
	
	//if there are no legal moves, return NULL
	if (!b.hasMoves(s)) { return NULL;}
	//if we are out of time, return NULL
	else if (msLeft >= 0 && msLeft < 10) { return NULL;}
	else{
		//create a list of moves to store legal moves
		std::list<Move> legalMoves;
		//iterate through all possible moves, adding them to legalMoves
		//if the move is legal
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				Move move(i, j);
				if (b.checkMove(&move, s))
				{
					legalMoves.push_back(move);
				}
			}
		}
		//set up variables to use when comparing score
		Board * b1, * b2;
		Move * optimum = &(*(legalMoves.begin()));
		optimum->score = -1000000;
		for (std::list<Move>::iterator it = legalMoves.begin(); it != legalMoves.end(); it++)
		{
			//copy boards into new variables and implement moves to compare
			b1 = b.copy();
			b2 = b.copy();
			b1->doMove(&(*it), s);
			b2->doMove(optimum, s);
			
			//assign weights to scores based on board position
			if((it->x == 0 && it->y == 0) || (it->x == 0 && it->y == 7) || (it->x == 7 && it->y == 0) || (it->x == 7 && it->y == 7))
			{
				it->score = 3 * b1->count(s);
			}
			else if((it->x == 1 && it->y == 1) || (it->x == 1 && it->y == 6) || (it->x == 6 && it->y == 1) || (it->x == 6 && it->y == 6))
			{
				it->score = -3 * b1->count(s);
			}
			else if((it->x == 0 && (it->y == 1 || it->y == 6)) || (it->x == 7 && (it->y == 1 || it->y == 6)) || (it->y == 0 && (it->x == 1 || it->x == 6)) || (it->y == 7 && (it->x == 1 || it->x == 6)))
			{
				it->score = -2 * b1->count(s);
			}
			else if(it->x == 0 || it->y == 0 || it->x == 7 || it->y == 7)
			{
				it->score = 2 * b1->count(s);
			}
			else if(it->x == 1 || it->y == 1 || it->x == 6 || it->y == 6)
			{
				it->score = -2 * b1->count(s);
			}
			else
			{
				it->score = b1->count(s);
			}
			
			//compare scores of the moves we are comparing and set the
			//pointer to the move with the highest score equal to optimum
			if(it->score >= optimum->score) { optimum = &(*it);}			
		}
		//return the move in LegalMoves with the highest score
		return optimum;
	}	
}
*/
